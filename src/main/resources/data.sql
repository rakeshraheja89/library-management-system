INSERT INTO book(id,book_author ,book_title ,category_id ,current_status) VALUES (1,'Jason','RAKESH','1','AVAILABLE');
INSERT INTO book(id,book_author ,book_title ,category_id ,current_status) VALUES (2,'Jason','Satya','2','AVAILABLE');

INSERT INTO book_category(id,name) VALUES (1,'JAVA');
INSERT INTO book_category(id,name) VALUES (2,'ENGLISH');


INSERT INTO book_history(id,book_id,borrowed_date,penalty,returned_date,user_id) VALUES (1,1,sysdate,500,sysdate,1);
INSERT INTO book_history(id,book_id,borrowed_date,penalty,returned_date,user_id) VALUES (2,2,sysdate,500,sysdate,2);

commit;