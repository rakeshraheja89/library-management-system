package com.library.management.system.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.library.management.system.model.BookHistory;

public interface BookHistoryRepository extends JpaRepository<BookHistory, Integer> {

}