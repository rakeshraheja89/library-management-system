package com.library.management.system.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.library.management.system.model.User;

public interface UserRepository extends CrudRepository<User, Long> {

	Optional<User> findByUserName(String string);

}
