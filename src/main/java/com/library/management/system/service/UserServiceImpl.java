package com.library.management.system.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.library.management.system.constant.ApplicationConstant;
import com.library.management.system.dto.ResponseDto;
import com.library.management.system.dto.UserDto;
import com.library.management.system.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	UserRepository userRepository;

	/**
	 * login 
	 */
	@Override
	public ResponseDto login(UserDto userDto) {
		logger.info("UserServiceimpl login()");
		return userRepository.findByUserName(userDto.getUserName()).map(user -> {
			if(user.getUserPassword().equals(userDto.getPassword()))
			{
				return new ResponseDto(200,ApplicationConstant.LOGIN_SUCCESS);
			}
			return new ResponseDto(400,ApplicationConstant.USER_NAME_PASSWORD_WRONG);
		}).orElse(new ResponseDto(400,ApplicationConstant.USER_NOT_EXISTS))	;	
		
	}

}
