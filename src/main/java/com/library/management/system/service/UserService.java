package com.library.management.system.service;


import com.library.management.system.dto.ResponseDto;
import com.library.management.system.dto.UserDto;

public interface UserService {

	ResponseDto login( UserDto userDto);

}
