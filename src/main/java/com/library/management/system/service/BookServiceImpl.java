
package com.library.management.system.service;

import java.nio.file.attribute.UserPrincipalNotFoundException;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.library.management.system.dto.BookBorrowDto;
import com.library.management.system.dto.BookReturnDto;
import com.library.management.system.exception.BooksException;
import com.library.management.system.exception.UserNotFoundException;
import com.library.management.system.model.Book;
import com.library.management.system.model.BookHistory;
import com.library.management.system.model.User;
import com.library.management.system.repository.BookHistoryRepository;
import com.library.management.system.repository.BookRepository;
import com.library.management.system.repository.UserRepository;

@Service
public class BookServiceImpl implements BookService {

	@Autowired
	BookRepository bookRepository;
	
	
	@Autowired
	BookHistoryRepository bookHistoryRepository;
	
	@Autowired
	UserRepository userRepository;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	
	@Override
	public BookHistory getBooksHistory(int userId) {
		Optional<BookHistory> bookObj = bookHistoryRepository.findById(userId);
		if (bookObj.isPresent()) {
			return bookObj.get();
		} else {
			//custom message
			return null;
		}
	}


	@Transactional
	public BookBorrowDto borrowBook(Integer userId, Integer bookId) {
		User user = userRepository.findById(Long.valueOf(userId)).orElseThrow(()-> new UserNotFoundException("UserId not found system"));
		Book book = bookRepository.findById(bookId).orElseThrow(()->new BooksException("Book not found exception"));
		BookHistory bookHistory = new BookHistory();
		bookHistory.setBookId(bookId);
		bookHistory.setUserId(userId);
		bookHistory.setBorrowedDate(new Date());
		book.setCurrentStatus("Borrowed");
		bookRepository.save(book);
		bookHistoryRepository.save(bookHistory);
		
		return new BookBorrowDto(book.getBookTitle(),user.getUserName(),book.getCurrentStatus(),getReturnDate());
	}


	private Date getReturnDate() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, 2);
		return cal.getTime();
	}


	@Transactional
	public BookReturnDto returnBooks(Integer userId, Integer bookId) {
		User user = userRepository.findById(Long.valueOf(userId)).orElseThrow(()-> new UserNotFoundException("UserId not found system"));
		Book book = bookRepository.findById(bookId).orElseThrow(()->new BooksException("Book not found exception"));
		BookHistory bookHistory = new BookHistory();
		bookHistory.setBookId(bookId);
		bookHistory.setUserId(userId);
		bookHistory.setReturnedDate(new Date());
		bookHistory.setPenalty(10l);
		book.setCurrentStatus("Returned");
		bookRepository.save(book);
		bookHistoryRepository.save(bookHistory);
		return new BookReturnDto();
	}



}
