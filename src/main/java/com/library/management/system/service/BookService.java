package com.library.management.system.service;

import com.library.management.system.dto.BookBorrowDto;
import com.library.management.system.dto.BookReturnDto;
import com.library.management.system.model.BookHistory;

public interface BookService {

	BookHistory getBooksHistory(int userId);

	BookBorrowDto borrowBook(Integer userId, Integer bookId);

	BookReturnDto returnBooks(Integer userId, Integer bookId);
	
}
