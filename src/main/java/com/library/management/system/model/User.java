package com.library.management.system.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "USER")
public class User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long userId;

	@NotNull
	@Column(name = "USER_NAME", unique = true)
	private String userName;
	
	@Column(name = "USER_EMAIL", unique = true)
	@Email
	private String userEmail;

	@NotNull
	@Column(name = "USER_PASSWORD")
	private String userPassword;

	@NotNull
	@Column(name = "USER_MOBILE_NUMBER")
	private Long userMobileNumber;

}
