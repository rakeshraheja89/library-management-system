package com.library.management.system.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table (name="Book")
public class Book   {
    /**
	 * 
	 */
	
	@Id  
    @Column 
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
	
	@Column(name="book_title")
	private String bookTitle;
	
	@Column(name="book_author")
	private String bookAuthor;
	
	@Column(name="category_Id")
	private int categoryId;
	
	@Column(name="current_status")
	private String currentStatus;


}
