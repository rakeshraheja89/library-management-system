package com.library.management.system.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name="Book_Category")
public class BookCategory {

	@Id  
    @Column 
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
	
    @Column(name="name") 
	private String name;
    
    
}