package com.library.management.system.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data  
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table (name="Book_history")
public class BookHistory   {
    /**
	 * 
	 */
	
	@Id  
    @Column 
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
	
	@Column(name="Book_ID")
    private Integer bookId;
	
	@Column(name="Borrowed_Date")
	private Date borrowedDate;
	
	@Column(name="Returned_Date")
	private Date returnedDate;
    
	@Column(name="User_ID")
    private Integer userId;
    
	@Column(name="penalty")
    private Long penalty;

	
}
