package com.library.management.system.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.library.management.system.dto.ResponseDto;

import com.library.management.system.dto.UserDto;
import com.library.management.system.service.UserService;

@RestController
@RequestMapping("/users")
public class UserController {
	
	@Autowired
	UserService userService;
	
	/**
	 * login api call
	 * @param userDto
	 * @return
	 */
	@PostMapping("/login")
	public ResponseEntity<ResponseDto>  login(@Valid @RequestBody UserDto userDto) {
		return new ResponseEntity<ResponseDto>(userService.login(userDto), HttpStatus.OK);
	}

}
