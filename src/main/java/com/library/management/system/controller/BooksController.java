package com.library.management.system.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.library.management.system.dto.BookBorrowDto;
import com.library.management.system.dto.BookReturnDto;
import com.library.management.system.model.Book;
import com.library.management.system.model.BookHistory;
import com.library.management.system.service.BookService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@RestController
@RequestMapping("/books")
public class BooksController {
	
	@Autowired
	BookService bookService;
	
	@Operation(summary = "/info/version")
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "/info/version ") })
	@GetMapping("/info/version")
	public String checkVersion() {
		return "Greetings from Spring Boot!";
	}
	
	@GetMapping("/borrowed/history/{userId}")
	//@Operation(summary = "/get")
	//@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "/get") })
	public ResponseEntity<BookHistory> getBooksHistory(@PathVariable Integer userId) {
		return new ResponseEntity<BookHistory>(bookService.getBooksHistory(userId),HttpStatus.OK);
		//return "Greetings from Spring Boot!";
	}
	
	@PutMapping("/borrow/users/{userId}/books/{bookId}")
	@Operation(summary = "/get")
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "/get") })
	public ResponseEntity<BookBorrowDto> borrowBooks(@PathVariable Integer userId ,@PathVariable Integer bookId) {
		return new ResponseEntity<BookBorrowDto>(bookService.borrowBook(userId,bookId),HttpStatus.OK);
	}
	
	@PutMapping("/return/userId/{userId}/bookId/{bookId}")
	public ResponseEntity<BookReturnDto> returnBooks(@PathVariable Integer userId ,@PathVariable Integer bookId) {
		return new ResponseEntity<BookReturnDto>(bookService.returnBooks(userId,bookId),HttpStatus.OK);
		
	}
}
