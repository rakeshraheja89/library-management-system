package com.library.management.system.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookBorrowDto {
	
	private String userName;
	private String bookName;
	private String bookStatus;
	private Date returnDate;

}
